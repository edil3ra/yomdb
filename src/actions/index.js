import {
  ERROR_SEARCHED_MOVIE,
  FETCHING_SEARCHED_MOVIE,
  LOADED_SEARCHED_MOVIE,
  ADD_MOVIE,
  TOGGLE_WATCHED_MOVIE,
  REMOVE_MOVIE,
  GENRE_FILTER_MOVIE,
  ACTOR_FILTER_MOVIE,
  WATCHED_TOGGLE_MOVIE
} from '../actionTypes'

import { mdbApiSearch } from '../api'

export function fetchMovieAction(title) {
  return function (dispatch) {
    dispatch({type: FETCHING_SEARCHED_MOVIE})
    return  mdbApiSearch(title)
      .then(response => {
        if (response.data.Response !== "False") {
          dispatch({
            type: LOADED_SEARCHED_MOVIE,
            data: {
              id: response.data.imdbVotes,
              title: title,
              plot: response.data.Plot,
              genres: response.data.Genre.split(',').map((s) => s.trim().toLowerCase()),
              actors: response.data.Actors.split(',').map((s) => s.trim().toLowerCase()),
            }
          })
        }
        else {
          dispatch({
            type: ERROR_SEARCHED_MOVIE,
            data: {
              title: title,
              error: response.data
            },
          })  
        }
      })
      .catch(err => {
        console.log(err)
      }) 
  }
}


export function addMovieAction(movie) {
  return function (dispatch) {
    dispatch({
      type: ADD_MOVIE,
      data: {...movie, watched: false}
    })
  }
}


export function toggleWatchedMovieAction(id) {
  return function (dispatch) {
    dispatch({
      type: TOGGLE_WATCHED_MOVIE,
      id: id
    })
  }
}


export function removeWatchedMovieAction(id) {
  return function (dispatch) {
    dispatch({
      type: REMOVE_MOVIE,
      id: id
    })
  }
}



export function setGenreFilterAction(string) {
  return function (dispatch) {
    dispatch({
      type: GENRE_FILTER_MOVIE,
      data: string
    })
  }
}


export function setActorFilterAction(string) {
  return function (dispatch) {
    dispatch({
      type: ACTOR_FILTER_MOVIE,
      data: string
    })
  }
}


export function togggleWatchedAction() {
  return function (dispatch) {
    dispatch({
      type: WATCHED_TOGGLE_MOVIE,
    })
  }
}
