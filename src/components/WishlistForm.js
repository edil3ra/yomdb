import React from 'react'
import PropTypes from 'prop-types'

const WishlistForm = ({
  genreOptions,
  actorOptions,
  watched,
  onSelectGenre,
  onSelectActor,
  onToggleWatched
}) => {
  const genreOptionsRender = genreOptions.map((genre) => {
    return (
      <option key={genre} value={genre}>{genre}</option>
    )
  })

  const actorOptionsRender = actorOptions.map((actor) => {
    return (
      <option key={actor} value={actor}>{actor}</option>
    )
  })

  const handleOnSelectGenre = (event) => {
    event.preventDefault()
    onSelectGenre(event.target.value)
  }

  const handleOnSelectActor = (event) => {
    event.preventDefault()
    onSelectActor(event.target.value)
  }

  const handleOnToggleWatched  = (event) => {
    onToggleWatched()
  }

  return (
    <div>
      <div className="column is-12">
        <h2 className="title">Movie List</h2>
      </div>
      <div className="column is-12">
        <div className="field is-grouped">
          <div className="control ">
            <div className="select is-primary">
              <select onChange={handleOnSelectGenre}>
                <option value="all">All genres</option>
                {genreOptionsRender}
              </select>
            </div>
          </div>
          <div className="control ">
            <div className="select is-primary">
              <select onChange={handleOnSelectActor}>
                <option value="all">All actors</option>
                {actorOptionsRender}
              </select>
            </div>
          </div>
          <p className="control">
            <input type="checkbox" onChange={handleOnToggleWatched}/> Watched
          </p>
        </div>
      </div>
    </div>
  )
}

WishlistForm.propTypes = {
  genreOptions: PropTypes.array,
  actorOptions: PropTypes.array,
  watched: PropTypes.bool,
  onSelectGenre: PropTypes.func,
  onSelectActor: PropTypes.func,
  onToggleWatched: PropTypes.func
}


export default WishlistForm
