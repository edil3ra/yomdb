import React from 'react'
import PropTypes from 'prop-types'

const SearchForm = ({ onSubmit }) => {
  let input

  const handleSubmit = (event) => {
    event.preventDefault()
    if (!input.value.trim()) {
      return
    }
    onSubmit(input.value)
    input.value = ''
  }

  return (
      <div>
        <h2 className="title">
          Search movies
        </h2>
        <form action="">
          <div className="field">
            <label className="label">Title</label>
            <div className="control">
              <input
                name ="title"
                className="input"
                type="text"
                placeholder="Search by title"
                ref={node => {input = node}}
              />
            </div>
            <p className="help">Search by title</p>
          </div>
          <div className="control">
            <button className="button is-primary" onClick={handleSubmit}> Search </button>
          </div>
        </form>
      </div>
  )
}

SearchForm.propTypes = {
  onSubmit: PropTypes.func
}

export default SearchForm
