import React from 'react'

const Header = () => {
  return (
    <header className="header">
      <nav className="navbar is-primary" role="navigation" aria-label="main navigation">
        <div className="container">
          <div className="navbar-brand">
            <h1 className="my-title">
              <a className="navbar-item">
                Watch List
              </a>              
            </h1>
            <a role="button" className="navbar-burger" aria-label="menu" aria-expanded="false">
              <span aria-hidden="true"></span>
              <span aria-hidden="true"></span>
              <span aria-hidden="true"></span>
            </a>
          </div>
        </div>
      </nav>
    </header>
  )
}
export default Header
