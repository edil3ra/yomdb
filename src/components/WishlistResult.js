import React from 'react'
import PropTypes from 'prop-types'

const WishlistResult = ({movies, onClickWatched, onClickRemoved}) => {
  const renderMovies = movies.map(({id, title, genres, actors, watched}) => {
    const genresString = genres.join(', ')
    const actorsString = actors.join(', ')
    const handleCollapseClick = (event) => {
      let el = event.currentTarget.parentElement.nextElementSibling
      el.classList.toggle('is-hidden')
    }

    const handleToggleShowClick = (id) => {
      onClickWatched(id)
    }

    const handleRemoveClick = (id) => {
      onClickRemoved(id)
    }
    
    const panelIconClass = watched ? 'has-background-light has-text-black': 'has-background-white has-text-black'
    return (
      <a key={id} className={`panel-block ${panelIconClass}`}>
        <div className={`columns is-multiline `} style={{width: '100%'}}>
          <div className='column is-12'>
            <span className="panel-icon" onClick={(event) => handleToggleShowClick(id)}>
              <i className='fas fa-clock has-text-warning' aria-hidden="true"></i>
            </span>
            <span className="panel-icon" onClick={(_) => handleRemoveClick(id)}>
              <i className='fas fa-times has-text-danger' aria-hidden="true"></i>
            </span>
            <span style={{textTransform: 'capitalize'}}>{title} </span>
            <span className="panel-icon is-pulled-right" onClick={handleCollapseClick}>
              <i className="fas fa-angle-down is-pulled-right is-medium" aria-hidden="true"> </i>
            </span>
          </div>
          <div className='column is-12 is-hidden'>
            <div>
              <strong>Genres: </strong> {genresString}
            </div>
            <div>
              <strong>Actors: </strong> {actorsString}
            </div>
          </div>
        </div>
      </a>
    )
  })

  return (
    <div className="column is-12">
      <div className="panel my-panel">
        {renderMovies}
      </div>
    </div>
  )
}

WishlistResult.propTypes = {
  movies: PropTypes.array,
  onClickWatched: PropTypes.func,
  onClickRemoved: PropTypes.func
}

export default WishlistResult
