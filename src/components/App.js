import React from 'react'
import './App.css'
import Header from './Header'
import SearchForm from './SearchForm'
import SearchResult from './SearchResult'
import WishlistForm from './WishlistForm'
import WishlistResult from './WishlistResult'
import SearchFormContainer from '../containers/SearchFormContainer'
import SearchResultContainer from '../containers/SearchResultContainer'
import WishlistResultContainer from '../containers/WishlistResultContainer'
import WishlistFormContainer from '../containers/WishlistFormContainer'


function App() {
  return (
    <div className="App">
      <Header />
      <section className="section content">
        <div className="container">
          <div className="columns">
            <div className="column is-7">
              <SearchFormContainer />
            </div>
            <div className="column is-5">
              <SearchResultContainer />
            </div>
          </div>
        </div>
      </section >
      <hr/>
      <section className="section">
        <div className="container">
          <div className="columns is-multiline">
            <WishlistFormContainer />
            <WishlistResultContainer />
          </div>
        </div>
      </section>
    </div>
  )
}

export default App
