import React from 'react'
import PropTypes from 'prop-types'

const SearchResult = ({ search, onClick }) => {

  const {title} = search
  let toRender = null

  if (Object.keys(search).length === 1 && search.error === null) {
    toRender = (
      <div>
        <h2 className="title"> No Movie searched yet... </h2>
      </div>
    )
  }  
  else if (search.error !== null) {
    toRender = (
      <div>
        <h3>No Result Found for {title}</h3>
      </div>      
    )
  } else {
    const {id, actors, genres, plot} = search

    const handleClick = (event) => {
      event.preventDefault()
      onClick({
        id: id,
        title: title,
        actors: actors,
        genres: genres,
        watched: false
      })
    }

    const genresString = genres.join(', ')
    const actorsString = actors.join(', ')
    toRender = (
      <div>
        <h3 className="">Result for <strong>{title}</strong></h3>
        <h4>Plot:</h4>
        <p>{plot}</p>
        <h4>Actors:</h4>
        <p>{actorsString}</p>
        <h4>Genres:</h4>
        <p>{genresString}</p>
        <button className="button is-primary"
                onClick={handleClick}>
          Add to wishlist
        </button>
      </div>      
    )
  }


  return (
    <div>
      <h2 className="title"> Search Result </h2>
      {toRender}
    </div>
  )
}

SearchResult.propTypes = {
  search: PropTypes.shape({
    id: PropTypes.string,
    title: PropTypes.string,
    plot: PropTypes.string,
    actors: PropTypes.array,
    genres: PropTypes.array,
    error: PropTypes.object
  }),
  onClick: PropTypes.func
}

export default SearchResult
