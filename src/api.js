import { mdbApiKey, mdbBaseUrl } from './config'

const axios = require('axios')


let mdbApi = undefined


function initApi () {
  if (!mdbApi) {
    mdbApi = axios.create({
      baseURL: mdbBaseUrl,
      timeout: 1000,
    })
  }
}

initApi()

function mdbApiSearch(title) {
  return mdbApi.request({
    method: 'GET',
    params: {
      t: title,
      apiKey: mdbApiKey
    }
  })
}

export {
  mdbApiSearch
}
