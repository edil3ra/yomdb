import { combineReducers } from 'redux'
import {
  ERROR_SEARCHED_MOVIE,
  FETCHING_SEARCHED_MOVIE,
  LOADED_SEARCHED_MOVIE,
  ADD_MOVIE,
  REMOVE_MOVIE,
  TOGGLE_WATCHED_MOVIE,
  GENRE_FILTER_MOVIE,
  ACTOR_FILTER_MOVIE,
  WATCHED_TOGGLE_MOVIE
} from '../actionTypes'

const searchedMovie = (state = {
  isFetching: false,
  data: null,
  error: null
}, action) => {
  switch (action.type) {
    case FETCHING_SEARCHED_MOVIE:
      return {...state, isFetching: true}
    case LOADED_SEARCHED_MOVIE:
      return {...state, data: action.data, error: null, isFetching: false}
    case ERROR_SEARCHED_MOVIE:
      return {
        ...state,
        error: action.data.error,
        data: {title: action.data.title},
        isFetching: false
      }
    default:
      return state
  }
}

const movies = (state = {
  items: {},
  genreFilter: 'all',
  actorFilter: 'all',
  watched: false
}, action) => {
  switch (action.type) {
    case ADD_MOVIE:
      const {id, ...movie} = action.data
      const updatedItems = {...state.items, [id]: movie}
      return {...state, items: updatedItems}
    case TOGGLE_WATCHED_MOVIE:
      return {...state, items: {
        ...state.items, [action.id]: {
          ...state.items[action.id], watched: !state.items[action.id].watched
        }
      }}
    case REMOVE_MOVIE: 
      const removedItems = {...state.items}
      delete removedItems[action.id]
      return {...state, items: removedItems}
    case GENRE_FILTER_MOVIE:
      return {...state, genreFilter: action.data}
    case ACTOR_FILTER_MOVIE:
      return {...state, actorFilter: action.data}
    case WATCHED_TOGGLE_MOVIE:
      return {...state, watched: !state.watched}
    default:
      return state
  }
}


export default combineReducers({
  searchedMovie,
  movies
})
