import { connect } from 'react-redux'
import { setGenreFilterAction, setActorFilterAction, togggleWatchedAction } from '../actions'
import WishlistForm from '../components/WishlistForm'


const mapStateToProps = (state, ownProps) => {

  const genres = Object.values(state.movies.items).reduce((xs, {genres}) => {
    return [...xs, ...genres]
  }, []) 

  const actors = Object.values(state.movies.items).reduce((xs, {actors}) => {
    return [...xs, ...actors]
  }, []) 

  return {
    genreOptions: Array.from(new Set(genres.values())),
    actorOptions: Array.from(new Set(actors.values())),
    wathed: state.watched
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    onSelectGenre: (string) => {
      dispatch(setGenreFilterAction(string))
    },
    onSelectActor: (string) => {
      dispatch(setActorFilterAction(string))
    },
    onToggleWatched: () => {
      dispatch(togggleWatchedAction())
    }
  }
}

const WishlistFormContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(WishlistForm)

export default WishlistFormContainer
