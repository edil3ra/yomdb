import { connect } from 'react-redux'
import { fetchMovieAction } from '../actions'
import SearchForm from '../components/SearchForm'


const mapDispatchToProps = dispatch => {
  return {
    onSubmit: (title) => {
      dispatch(fetchMovieAction(title))
    }
  }
}

const SearchFormContainer = connect(
  null,
  mapDispatchToProps
)(SearchForm)

export default SearchFormContainer
