import { connect } from 'react-redux'
import { toggleWatchedMovieAction, removeWatchedMovieAction } from '../actions'
import WishlistResult from '../components/WishlistResult'


const mapStateToProps = (state, ownProps) => {
  const movies = Object.entries(state.movies.items).map(([key, value]) => {
    return {id: key, ...value}
  })

  const {genreFilter, actorFilter, watched: watchedFilter} = state.movies
  const watchedMovies = movies.filter(({watched}) => watched === watchedFilter)
  const genreMovies = watchedMovies.filter(({genres}) => {
    return genres.find((genre) =>  genre === genreFilter || genreFilter === 'all')
  })
  const actorMovies = genreMovies.filter(({actors}) => {
    return actors.find((actor) =>  actor === actorFilter || actorFilter === 'all')
  })

  
  return {
    movies: actorMovies
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    onClickWatched: (id) => {
      dispatch(toggleWatchedMovieAction(id))
    },
    onClickRemoved: (id) => {
      dispatch(removeWatchedMovieAction(id))
    }
  }
}

const WishlistResultContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(WishlistResult)

export default WishlistResultContainer
