import { connect } from 'react-redux'
import { addMovieAction } from '../actions'
import SearchResult from '../components/SearchResult'


const mapStateToProps = (state, ownProps) => {
  return {
    search: {...state.searchedMovie.data, error: state.searchedMovie.error},
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    onClick: (movie) => {
      dispatch(addMovieAction(movie))
    }
  }
}

const SearchResultContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(SearchResult)

export default SearchResultContainer
