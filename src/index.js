import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'
import * as serviceWorker from './serviceWorker'
import 'bulma/css/bulma.min.css'
import { Provider } from 'react-redux'
import thunkMiddleware from 'redux-thunk'
import { createStore, applyMiddleware } from 'redux'
import App from './components/App'
import rootReducer from './reducers'
import { composeWithDevTools } from 'redux-devtools-extension';
import { save, load } from "redux-localstorage-simple"


const createStoreWithMiddleware =
  composeWithDevTools(
    applyMiddleware(
      thunkMiddleware,
      save({states: ['movies']})
    )
  )(createStore)

const store = createStoreWithMiddleware(
  rootReducer,
  load({states: ['movies']})
)

window.store = store

ReactDOM.render(
  <Provider store={store}>
    <App />,
  </Provider>,
  document.getElementById('root')
)



// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister()
